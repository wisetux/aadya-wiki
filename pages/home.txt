====== Home ======

Here is a collection of various tips, tricks and tutorials I learnt dealing with tech - mostly Computers and Phones. 

===== Get Involved =====

If you want to help contribute information to this resource pool, then you’ve come to the right place. Keep reading to learn how.

You have just taken your first step toward getting involved. Before you get started, request you to please observe the [[:code-of-conduct|Code of Conduct]]. It’s not very long and it will help you get started.

